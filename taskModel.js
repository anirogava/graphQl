const mongoose = require('mongoose');
const { v4: uuidv4 } = require('uuid');

const Tasks = new mongoose.Schema({
    id: {
        type: String,
        default: uuidv4,
        unique: true,
    },
    title: String,
    description: String,
    tags: [String],
    done: Boolean,
});

module.exports = mongoose.model('Tasks', Tasks);
