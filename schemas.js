const {
    GraphQLObjectType,
    GraphQLString,
    GraphQLBoolean,
    GraphQLList,
    GraphQLSchema,
    GraphQLInputObjectType,
} = require('graphql');
const Task = require('./taskModel');

const TaskInputType = new GraphQLInputObjectType({
    name: 'TaskInput',
    fields: {
        title: { type: GraphQLString },
        description: { type: GraphQLString },
        tags: { type: new GraphQLList(GraphQLString) },
        done: { type: GraphQLBoolean },
    },
});

const TaskType = new GraphQLObjectType({
    name: 'Task',
    fields: {
        id: { type: GraphQLString },
        title: { type: GraphQLString },
        description: { type: GraphQLString },
        tags: { type: new GraphQLList(GraphQLString) },
        done: { type: GraphQLBoolean },
    },
});

const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
        task: {
            type: TaskType,
            args: { id: { type: GraphQLString } },
            resolve(parent, args) {
                return Task.findById(args.id);
            },
        },
        tasks: {
            type: new GraphQLList(TaskType),
            resolve(parent, args) {
                return Task.find({});
            },
        },
    },
});

const Mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: {
        addTask: {
            type: TaskType,
            args: {
                input: { type: TaskInputType },
            },
            resolve(parent, args) {
                const task = new Task(args.input);
                return task.save();
            },
        },
        updateTask: {
            type: TaskType,
            args: {
                id: { type: GraphQLString },
                input: { type: TaskInputType },
            },
            resolve(parent, args) {
                return Task.findByIdAndUpdate(
                    args.id,
                    args.input,
                    { new: true }
                );
            },
        },
        deleteTask: {
            type: TaskType,
            args: {
                id: { type: GraphQLString },
            },
            resolve(parent, args) {
                return Task.findByIdAndRemove(args.id);
            },
        },
    },
});

const schema = new GraphQLSchema({
    query: RootQuery,
    mutation: Mutation,
});

module.exports = schema;
