const express = require('express');
const { graphqlHTTP } = require('express-graphql');
const schema = require('./schemas');
const mongoose = require('mongoose');
const dotenv = require('dotenv');


const app = express();
dotenv.config();

mongoose.connect(process.env.DB_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});

const connection = mongoose.connection;
connection.on('error', console.error.bind(console, 'MongoDB connection error:'));
connection.once('open', () => {
    console.log('Connected to MongoDB successfully!');
});

app.use('/graphql', graphqlHTTP({
    schema,
    graphiql: true,
}));

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Server is running on port 3000`);
});
